#include <jni.h>
#include <string.h>

JNIEXPORT jstring JNICALL
Java_com_rth_security_AppInfo_getApiEndpoint(JNIEnv *env,jobject instance, jstring key) {
        if(checkKey(env,key)==1)
            {
                return (*env)->NewStringUTF(env, "https://your_server_address");
            }
        else
            {
                return (*env)->NewStringUTF(env, "");
            }
}

JNIEXPORT jstring JNICALL
Java_com_rth_security_AppInfo_getApiToken(JNIEnv *env, jobject instance, jstring key) {
        if(checkKey(env,key)==1)
            {
                return (*env)->NewStringUTF(env, "your_private_key");
            }
        else
            {
                return (*env)->NewStringUTF(env, "");
            }

}

int checkKey(JNIEnv *env,jstring key){
    const char *nativeKey = (*env)->GetStringUTFChars(env, key, NULL);
    if(strcmp(nativeKey,"fill_hashkey_of_keystore_here")==0)
        {
            (*env)->ReleaseStringUTFChars(env, key, nativeKey);
            return 1;
        }
    else
        {
            (*env)->ReleaseStringUTFChars(env, key, nativeKey);
            return 0;
        }
}
