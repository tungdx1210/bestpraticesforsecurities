package com.rth.security;

/**
 * Created by TUNGDX on 9/14/2016.
 */
public class AppInfo {
    static {
        System.loadLibrary("secure");
    }

    public static native String getApiEndpoint(String key);

    public static native String getApiToken(String key);
}
