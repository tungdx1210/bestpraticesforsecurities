package com.rth.security;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        TextView server = (TextView) findViewById(R.id.server_address);
        TextView privateKey = (TextView) findViewById(R.id.private_key);

        String key = Utils.getHashKey(this);
        server.setText(getString(R.string.server, AppInfo.getApiEndpoint(key)));
        privateKey.setText(getString(R.string.private_key, AppInfo.getApiToken(key)));
    }
}
